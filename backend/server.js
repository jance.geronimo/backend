import express from "express";
import morgan from "morgan";
import todoRoutes from "./routes/todo.js";

const app = express();

app.use(express.static("public"));

const server = app.listen(8081, function () {
  //   var host = server.address().address;
  var host = "localhost";
  var port = server.address().port;

  console.log("Example app listening at http://%s:%s", host, port);
});

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.use(express.json());
app.use(morgan("dev"));

// app.use("/api/todo", todoRoutes);
app.use("", todoRoutes);
