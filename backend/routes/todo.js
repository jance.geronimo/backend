import express, { application } from "express";
import { z } from "zod";
import { todoCollection } from "../db.js";
import { ObjectId } from "mongodb";

const router = express.Router();

const TodoSchema = z.object({
  // id: z.number(),
  task: z.string(),
  checked: z.boolean(),
});

// Values of todo when run
let todo = [
  { id: 1, task: "To Study", checked: false },
  { id: 2, task: "To Play", checked: false },
  { id: 3, task: "To Code", checked: true },
];

// url = /todos | method = GET
// Get all todo
router.get("/todos", async (req, res) => {

  // get data from MongoDB
  const todo = await todoCollection.find().toArray()

  res.status(200).send(todo);
});

// url = /todos/{id} | method = GET
// Get todo based on id
router.get("/todos/:id", async (req, res) => {
  const todoId = req.params.id;

  if(!ObjectId.isValid(todoId)) return res.status(400).send('ID is not valid.')

  const foundTodo = await todoCollection.findOne({
    _id: new ObjectId(todoId)
  })

  console.log(foundTodo)

  //check if todo is found
  if(foundTodo == null) return res.send(404).send('Not Found')
  
  res.status(200).send(foundTodo);

  // // findIndex returns -1 if not found and if found return the indexed location
  // const foundIndex = todo.findIndex((ci) => ci.id === Number(todoId));

  // if (foundIndex === -1) {
  //   res.status(404).send("<p>Id doesn't Exist</p>");
  // } else {
  //   res.status(200).send(todo[foundIndex]);
  // }
});

// url = /todos | method = POST
// Create a todo
router.post("/todos", async (req, res) => {

  // const newTodo = { ...req.body, id: new Date().getTime(), checked: false };
  // create and store in MongoDB
  const newTodo = req.body;
  const parsedResult = TodoSchema.safeParse(newTodo);

  if (!parsedResult.success) {
    return res.status(400).send(parsedResult.error);
  }

  const result = await todoCollection.insertOne(parsedResult.data)

  const todoList = await todoCollection.findOne({
    _id: new ObjectId(result.insertedId)
  })

  // todo = [...todo, parsedResult.data];
  // res.status(201).send(parsedResult.data);

  res.status(201).send(todoList)
});

// url = /todos/{id} method = PATCH(POST)
// Update the checked status of a todo
router.patch("/todos/:id", async (req, res) => {
  const todoId = req.params.id;
  const task = req.body.task;
  const checked = req.body.checked;

  // check if todoId is valid
  if(!ObjectId.isValid(todoId)) return res.status(400).send('ID is not valid.')
  const foundTodoList = await todoCollection.findOne({
    _id: new ObjectId(todoId)
  })

  console.log(foundTodoList)

  //check if todo is found
  if(foundTodoList == null) return res.send(404).send('Not Found')
  
  todoCollection.updateOne({
    _id: new ObjectId(todoId)
  }, {
    // include task inside if you want to update task
    $set: { checked }
  })

  const updatedTodo = await todoCollection.findOne({
    _id: new ObjectId(todoId)
  })

  res.status(200).send(updatedTodo)

  // const foundIndex = todo.findIndex((ci) => ci.id === Number(todoId));

  // if (foundIndex === -1) {
  //   return res.status(404).send("Not Found");
  // }
  // const newTodo = {
  //   ...req.body,
  //   id: Number(todoId),
  //   task: todo[foundIndex].task,
  // };
  // const parsedResult = TodoSchema.safeParse(newTodo);

  // if (!parsedResult.success) {
  //   return res.status(400).send(parsedResult.error);
  // }

  // todo[foundIndex].checked = req.body.checked;
  // res.status(200).send(todo[foundIndex]);
});

// url = /todos/{id} method = DELETE(POST)
// Delete a task using id
router.delete("/todos/:id", async (req, res) => {
  const todoId = req.params.id;
  
  if(!ObjectId.isValid(todoId)) return res.status(400).send('ID is not valid.')

  const foundTodoList = await todoCollection.findOne({
    _id: new ObjectId(todoId)
  })

  console.log(foundTodoList)

  const deletedTodo = await todoCollection.deleteOne({
    _id: new ObjectId(todoId)
  })
  res.status(204).send(deletedTodo)

  // const foundIndex = todo.findIndex((ci) => ci.id === Number(todoId));

  // if (foundIndex === -1) {
  //   return res.status(404).send("Not Found");
  // }

  // todo.splice(foundIndex, 1);

  // res.status(204).send();
});

export default router;
